from node:16.1-alpine
WORKDIR /todo-frontend
COPY package.json .
COPY yarn.lock .
RUN yarn install
COPY . .
EXPOSE 3000
CMD ["yarn", "start"]
