import axios from "axios";
console.log(process.env)
const config = {
  baseURL: process.env.REACT_APP_SERVER_URL
};

export const apiInstance = axios.create(config);
