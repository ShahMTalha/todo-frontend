from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

import os
from datetime import datetime
import time
curr_dir = os.getcwd()

options = Options()
options.add_argument('--headless')
options.add_argument('--disable-gpu')
# options.add_argument("--user-data-dir=/Users..../Google/Chrome")


driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get(os.environ.get('TEST_SERVER', 'http://3.129.62.132/'))

delay = 10 # seconds
try:
    myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'myDIV')))
    print("Page is ready!")
except TimeoutException:
    print("Loading took too much time!")

inputElement = driver.find_element_by_tag_name("input")
inputContent = 'Testing uat case ' + str(datetime.utcnow())
inputElement.send_keys(inputContent)
clickElement = driver.find_element_by_tag_name("span")
clickElement.click()
time.sleep(10)
listElement = driver.find_element_by_id("myUL")
listElement = driver.find_elements_by_xpath('//div[@id="root"]/div/ul')
innerELement = listElement[0].find_elements_by_xpath('//li')
outputContent = innerELement[0].text
print(outputContent)
print(inputContent)
assert inputContent == outputContent, "UAT case Failed"
print("UAT Completed")
driver.quit()