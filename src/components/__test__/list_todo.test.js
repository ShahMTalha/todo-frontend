import React from "react";
import { render, screen, cleanup } from "@testing-library/react";
import '@testing-library/jest-dom'
import TodoList from "../TodoList"

afterEach(() => {
    cleanup();
});

test('Should render listing component', ()=> {
    const listing = [{id: 1, content: 'Component test case', type:'complete', created_at:'01-01-2000 12:00:00'}]
    render(<TodoList list={listing}/>);
    const add_todo_Element = screen.getByTestId('todo-list');
    expect(add_todo_Element).toBeInTheDocument();
    expect(add_todo_Element).toHaveTextContent('Component test case');
});
