import React from "react";
import { cleanup, render, screen } from "@testing-library/react";
import '@testing-library/jest-dom'
import AddTodo from "../AddTodo"

afterEach(() => {
    cleanup();
});

test('Should run add component', ()=> {
    render(<AddTodo/>);
    const add_todo_Element = screen.getByTestId('todo-add');
    expect(add_todo_Element).toBeInTheDocument();
    expect(add_todo_Element).toContainHTML('<h2')
    expect(add_todo_Element).toContainHTML('<input')
    expect(add_todo_Element).toContainHTML('<span')
    expect(add_todo_Element).toHaveTextContent('My To Do List');
});
