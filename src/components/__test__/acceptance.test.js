import renderer from "react-test-renderer"
import { render, screen, cleanup } from "@testing-library/react";
import '@testing-library/jest-dom'
import TodoList from "./../TodoList"
import AddTodo from "./../AddTodo"
import App from "./../../App"

afterEach(() => {
    cleanup();
});

test('Should render listing component', ()=> {
    render(<App/>);
    const list_todo_Element = screen.getByTestId('todo-list');
    const add_todo_Element = screen.getByTestId('todo-add');
    expect(add_todo_Element).toBeInTheDocument();
    expect(list_todo_Element).toBeInTheDocument();
});

test('matches spanshot', ()=> {
    const listing = [{id: 1, content: 'Component test case', type:'complete', created_at:'01-01-2000 12:00:00'}]
    const tree= renderer.create(<TodoList list={listing}/>).toJSON();
    expect(tree).toMatchSnapshot()
});

test('matches spanshot', ()=> {
    const tree= renderer.create(<AddTodo/>).toJSON();
    expect(tree).toMatchSnapshot()
});