<div id="top"></div>



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/ShahMTalha/todo-frontend.git">
    <img src="images/todo.jpg" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Todo-FrontEnd README</h3>

  <p align="center">
    Todo Listing Frontend project!
    <br />
    <a href="https://gitlab.com/ShahMTalha/todo-frontend.git"><strong>Explore the docs »</strong></a>
    <br />
    <br />
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

This project is generally implemented to add todo and maintain the list. The project has some following key points

Here's why:
* Project is developed on test driven approach.
* This project has implemented a GitLab CI/CD covering the whole cycle.
* The pipeline manages deployment through docker to AWS EC2 instances (test and production)

Of course, this project is starting line So I'll be adding more in the near future. You may also suggest changes by forking this repo and creating a pull request or opening an issue.

Use the `README.md` to get started.

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

This app is developed in highly usable front end js library listed below.

* [React.js](https://reactjs.org/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

Before jumping into the project please ensure that node (pref 16.1) is installed on your machine that verify following prerequisites.
* Ubuntu
  ```sh
  sudo apt-get update -y
  apt install nodejs
  ```

### Installation

So please ensure to every step to clone the project and run it on your local machine successfully

1. Clone the repo
   ```sh
   git clone https://gitlab.com/ShahMTalha/todo-frontend.git
   ```
2. Install yarn and its dependencies
   ```js
   npm install --global yarn
   yarn 
   ```
3. Make sure to add base url environment variable or .env file within the react project
   ```ssh
   REACT_APP_SERVER_URL=your_base_server_url

   ```
4. Start Yarn to run the react project into your machine
   ```ssh
   yarn start
   ```
5. Test the unit and acceptance cases and before you have to ensure following libraries should be installed i.e. jest dom mainly used in react app unit/component and acceptance test case running (offers its own cli control, provide syntax to run or skip test cases and widely used in automated browser testing) 
   ```ssh
   yarn add @testing-library/react react-test-renderer
   yarn add @testing-library/jest-dom
   ```

   Following command will run the unit/component test cases along with acceptance test case implemented to compare the  code sanpshots and to run single test case file need to install jest cli and from src/components/__test__/* run any test.js: 
   ```ssh
   yarn test
   or 
   yarn global add jest-cli
   jest test_filename.js
   ```

   Moreover user acceptance test cases are written in python with selenium web driver for better coverage
   ```ssh
   pip3 install selenium
   python3 src/tests/uat/user_acceptance.py
   ```

   Contract base test will run through postman so better install it by following command and then import contract pact from src/test/contract_test/todo-contract.json to run apis for test coverage
   ```ssh
   sudo snap install postman
   ```
6. After completing all above dockerize your application locally. Firstly need to install docker:
   ```ssh
   sudo apt install docker.io -y
   ```
   If you want to store images on docker hub then login to your account and then you need to push and pull images on your docker account as well.
   ```ssh
   sudo docker login -u username -p password
   ```
   Then Dockerfile is present in the repository will be used to generate new image with tag latest once complete then push the image.
   ```ssh
   docker build -t image_name:latest .
   docker image push image_name:latest
   ```
   If the container name already exist then you need to remove it first and remove previous images as well.
   ```ssh
   docker ps -a
   docker rm -f container_name
   docker images 
   docker rmi -f image_id
   ```
   Then at very last pull the latest image and run the docker container by specifying port currently using 80:3000 for frontend to run it default port of your ip address.
   ```ssh
   docker pull image_name:latest 
   docker run -d -p 80:3000 -e REACT_APP_SERVER_URL=http://your_backend_host:5000/todo/ --name container_name image_name:latest
   ```
<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

So by performing the above you will be able to run this project into your own setup and can add or view the todos.

I have deployed this frontend app on AWS EC2 :

Test Server : 3.129.62.132
Production Server : 18.219.141.59

<p align="right">(<a href="#top">back to top</a>)</p>


## Contact

Shah Muhammad Talha Tahir - [@linkedin_profile](https://www.linkedin.com/in/shah-muhammad-talha-tahir-27604698/) - shahmuhammadtalhatahir@gmail.com

Project Link: [https://gitlab.com/ShahMTalha/todo-frontend.git](https://gitlab.com/ShahMTalha/todo-frontend.git)




<p align="right">(<a href="#top">back to top</a>)</p>


[product-screenshot]: images/screenshot.png
